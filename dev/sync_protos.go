package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

const bazelProtoTarget = "//proto/cardoncio:cardoncio_go_proto"
const eventJSONOut = "/tmp/bazel_cardoncio_proto_build.json"
const bazelPathPrefix = "/gitlab.com/hstefan/cardoncio/"

type buildEvent struct {
	ID struct {
		TargetCompleted struct {
			Label string
		}
	}
	Completed struct {
		Success      bool
		OutputGroups struct {
			Name string
		}
		ImportantOutput []struct {
			Name       string
			PathPrefix []string
		}
	}
}

// snippet from https://opensource.com/article/18/6/copying-files-go
func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func main() {
	verbose := false
	for _, arg := range os.Args[1:] {
		if arg == "-v" {
			verbose = true
		}
	}

	if workspaceDir := os.Getenv("BUILD_WORKSPACE_DIRECTORY"); workspaceDir != "" {
		// bazel run does not start the child in CWD, but exposes the workspace directory
		// see: https://github.com/bazelbuild/bazel/issues/3325#issuecomment-392591440
		os.Chdir(workspaceDir)
	}

	cmd := exec.Command("bazel", "build", bazelProtoTarget, "--output_groups=go_generated_srcs", "--build_event_json_file", eventJSONOut)
	var stdout, stderr bytes.Buffer
	cmd.Stderr = &stderr
	cmd.Stdout = &stdout
	err := cmd.Run()

	if verbose {
		fmt.Printf("[bazel stdout]\n%s", stdout.String())
		fmt.Printf("[bazel stderr]\n%s", stderr.String())
	}

	if err != nil {
		log.Fatalf("failed to invoke bazel: %v", err)
	}
	jsonFile, err := os.Open(eventJSONOut)
	if err != nil {
		log.Fatalf("could not open json file: %v", err)
	}

	dec := json.NewDecoder(jsonFile)
	for {
		var evt buildEvent
		if err := dec.Decode(&evt); err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("can't parse json: %v", err)
		}
		if evt.ID.TargetCompleted.Label == bazelProtoTarget {
			// we found the target complete event
			for _, importantPath := range evt.Completed.ImportantOutput {
				targetPath := strings.SplitAfter(importantPath.Name, bazelPathPrefix)[1]
				sourcePath := path.Join(path.Join(importantPath.PathPrefix...), importantPath.Name)
				nBytes, err := copy(sourcePath, targetPath)
				if err != nil {
					log.Fatalf("failed to copy %s: %v", sourcePath, err)
				}
				fmt.Printf("copied: %s to %s [%d bytes]\n", sourcePath, targetPath, nBytes)
			}
		}
	}
}
