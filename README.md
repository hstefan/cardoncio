# Cardoncio

Over-engineered chat bot services.

## Database setup

`cardondiod` connects to a PostgreSQL database managed with [Postgres Operator](https://access.crunchydata.com/documentation/postgres-operator/4.4.1).


Postgres Operator generates secrets for the credentials, and those were copied to the namespace were Cardoncio runs, using
something like this:

```bash
kubectl get -n pgo secrets/cardoncio-cardoncio-secret --export -o yaml > dbsecrets.yaml
vim dbsecrets.yaml # edit namespace and secret name manually
kubectl apply --namespace=default -f secret
```

The database tables were setup manually using:

```bash
kubectl -n pgo port-forward svc/cardoncio 6432:5432 # separate shell
kubectl -n pgo get secret cardoncio-cardoncio-secret -o jsonpath='{.data.password}' | base64 --decode # read the password
psql -p 6432 -h localhost -U cardoncio cardoncio < sql/cardoncio/schema.sql
```

## Telegram API

The API token was generated using Telegram's "Botfather" bot and deployed do the cluster using secrets:

```bash
kubectl create secret generic cardoncio-telegram-api-credentials --from-literal=token='REDACTED'
```

## Skaffold usage

Skaffold is used for development and deployment, with Bazel as the build backend. Deployment can be done as follows:

```bash
kubectl config use-context gke_cardoncio_us-central1-c_cluster-0
skaffold build -q  | skaffold deploy --build-artifacts -
```

## Updating dependencies

```bash
go get -u ./...
go mod tidy
bazel run //:gazelle -- update-repos -from_file=go.mod
```
