package main

import (
	"gitlab.com/hstefan/cardoncio/pkg/tgingestd"
)

func main() {
	command := tgingestd.NewTgIngestdCommand()
	command.Execute()
}
