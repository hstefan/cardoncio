package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/hstefan/cardoncio/pkg/cardonciod"
)

func main() {
	command := cardonciod.NewCardonciodCommand()
	command.Execute()
}
