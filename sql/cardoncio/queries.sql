-- name: FindUserByID :one
SELECT
    *
FROM
    users
WHERE
    id = $1
LIMIT 1;

-- name: PersistChannel :one
INSERT INTO "groups" (id, last_active)
    VALUES ($1, NOW())
ON CONFLICT (id)
    DO UPDATE SET
        last_active = NOW()
    RETURNING
        *;

-- name: PersistUser :one
INSERT INTO users (id, last_active, display_name)
    VALUES ($1, NOW(), $2)
ON CONFLICT (id)
    DO UPDATE SET
        last_active = NOW()
    RETURNING
        *;

-- name: PersistMembership :one
INSERT INTO group_users (user_id, group_id, last_active)
    VALUES ($1, $2, NOW())
ON CONFLICT (user_id, group_id)
    DO UPDATE SET
        last_active = NOW()
    RETURNING
        *;

-- name: ListChannelMembers :many
SELECT
    u.*
FROM
    group_users gu
    JOIN users u ON u.id = gu.user_id
    JOIN "groups" g ON g.id = gu.group_id
WHERE
    gu.group_id = $1;

-- name: FindMembershipByID :one
SELECT
    *
FROM
    group_users
WHERE
    id = $1
LIMIT 1;

-- name: FindMembership :one
SELECT
    *
FROM
    group_users
WHERE
    user_id = $1
    AND group_id = $2;

-- name: CreateReminder :one
INSERT INTO reminders (group_user_id, message_ref, scheduled_for, sent_on)
    VALUES ($1, $2, $3, $4)
RETURNING
    *;

-- name: FindDueReminders :many
SELECT
    r.*, gu.group_id, gu.user_id
FROM
    reminders r
    JOIN group_users gu ON gu.id = r.group_user_id
WHERE
    r.sent_on IS NULL
    AND r.scheduled_for <= NOW();

-- name: CreateQuote :one
INSERT INTO group_user_quotes (group_user_id, message_ref)
    VALUES ($1, $2)
RETURNING
    *;

-- name: ListUserQuotesOrderedByLastSentOn :many
SELECT
    guq.id,
    gu.user_id,
    guq.message_ref,
    guq.last_sent_on
FROM
    group_user_quotes guq
    JOIN group_users gu ON gu.id = guq.group_user_id
WHERE
    guq.group_user_id = $1
ORDER BY
    last_sent_on ASC NULLS FIRST;

-- name: FindQuoteByMessageRef :one
SELECT
    *
FROM
    group_user_quotes
WHERE
    group_user_id = $1
    AND message_ref = $2
LIMIT 1;

-- name: UpdateQuoteLastSentOn :exec
UPDATE
    group_user_quotes
SET
    last_sent_on = now()
WHERE
    id = $1;

-- name: DeleteUserQuote :execrows
DELETE FROM group_user_quotes
WHERE group_user_id = $1
    AND message_ref = $2;

-- name: UpdateUserTimezone :exec
UPDATE
    users
SET
    timezone = $2
WHERE
    id = $1;

-- name: UpdateReminderSentOn :exec
UPDATE
    reminders
SET
    sent_on = now()
WHERE
    id = $1;

-- name: FindGroupById :one
SELECT
    *
FROM
    groups
WHERE
    id = $1
LIMIT 1;

-- name: UpdateGroupQuoteInactivityThreshold :exec
UPDATE
    groups
SET
   quote_inactivity_threshold_ms = $2
WHERE
    id = $1;
