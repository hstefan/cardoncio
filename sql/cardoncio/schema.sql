---
--- Stores user data and configuration
---

CREATE TABLE IF NOT EXISTS users (
  id text PRIMARY KEY NOT NULL,
  display_name text,
  last_active timestamp WITHOUT time zone,
  timezone text
);

---
--- Placeholder for group-specific data
---

CREATE TABLE IF NOT EXISTS "groups" (
  id text PRIMARY KEY NOT NULL,
  last_active timestamp WITHOUT time zone,
  -- ideally we'd use interval, but that doesn't make clealy to Go's time.Duration
  quote_inactivity_threshold_ms BIGINT
);

---
--- Stores information about which users are known to be member of groups
---

CREATE TABLE IF NOT EXISTS group_users (
  id serial PRIMARY KEY NOT NULL,
  user_id text REFERENCES users NOT NULL,
  group_id text REFERENCES "groups" NOT NULL,
  last_active timestamp WITHOUT time zone
);

ALTER TABLE group_users
  ADD UNIQUE (user_id, group_id);

---
--- Stores reminders that are linked to specific messages that will be forwarded to a given
--- chat when the scheduled time arrives.
---

CREATE TABLE IF NOT EXISTS reminders (
  id serial PRIMARY KEY NOT NULL,
  group_user_id int REFERENCES group_users (id) NOT NULL,
  message_ref text NOT NULL,
  scheduled_for timestamp WITH time zone NOT NULL,
  sent_on timestamp WITHOUT time zone
);

---
--- Stores references to messages associated as a quote of a user in a group
---

CREATE TABLE IF NOT EXISTS group_user_quotes (
  id serial PRIMARY KEY NOT NULL,
  group_user_id int REFERENCES group_users (id) NOT NULL,
  message_ref text NOT NULL,
  last_sent_on timestamp WITHOUT time zone
);

ALTER TABLE group_user_quotes
  ADD UNIQUE (group_user_id, message_ref);
