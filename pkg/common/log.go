package common

import (
	"os"

	"github.com/sirupsen/logrus"
)

// Log is a configured logrus logger object
var Log = logrus.New()

func init() {
	Log.Out = os.Stdout
	Log.SetFormatter(&logrus.TextFormatter{})
}

// ReconfigureLog applies user-configured options read from config
func ReconfigureLog(options CommonOptions) {
	level, err := logrus.ParseLevel(options.LogLevel)
	if err != nil {
		Log.Errorf("unknown log level %s configured, defaulting to `info`", err)
		Log.SetLevel(logrus.InfoLevel)
	} else {
		Log.SetLevel(level)
	}
}
