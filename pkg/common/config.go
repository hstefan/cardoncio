package common

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

// CommonOptions is used to control running settings for cardoncio services
type CommonOptions struct {
	LogLevel string
}

// InitializeConfigSystem sets up shared defaults for Viper
func InitializeConfigSystem(appName string) {
	viper.SetEnvPrefix(strings.ToUpper(appName))
	viper.SetConfigName(appName)
	viper.SetConfigType("hcl")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./configs/")
	viper.AddConfigPath(fmt.Sprintf("/etc/%s/", appName))
	viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", appName))
	viper.AutomaticEnv()
	// default configuration values
	viper.SetDefault("log_level", "info")
}

// ReadConfigOrPanic attempts to load configuration and panics in it fails
func ReadConfigOrPanic() CommonOptions {
	err := viper.ReadInConfig()
	if err != nil {
		Log.Fatalf("fatal error reading config file: %s", err)
		panic("refusing to start due to configuration failure")
	}
	return CommonOptions{
		LogLevel: viper.GetString("log_level"),
	}
}
