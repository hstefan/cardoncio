package cardonciod

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/hstefan/cardoncio/pkg/common"
)

func init() {
	common.InitializeConfigSystem("cardonciod")
}

// DatabaseOptions contains settings for connecting to a databased
type DatabaseOptions struct {
	Host     string
	Port     int
	User     string
	Password string
	Name     string
}

// AsConnectionURL returns a connection string for the specified settings
func (d *DatabaseOptions) AsConnectionURL() string {
	return fmt.Sprintf("user=%s password=%s host=%s port='%d' dbname=%s sslmode=disable", d.User, d.Password, d.Host, d.Port, d.Name)
}

// DaemonOptions is used to control running settings for cardonciod
type DaemonOptions struct {
	CommonOpts     common.CommonOptions
	ListenAddr     string
	DatabaseOpts   DatabaseOptions
	FeatureOptions FeatureOptions
}

// FeatureOptions are settings related to features (ie not infrastructure or runtime related)
type FeatureOptions struct {
	InactivityThresholdSecs float64
	AdminUserID             string
}

// ReadConfig reads settings using viper and builds a DaemonOptions object
func ReadConfig() DaemonOptions {
	viper.SetDefault("listen_addr", ":5555")

	// set database connection defaults (`password` has no default)
	viper.SetDefault("pg_host", "localhost")
	viper.SetDefault("pg_port", 6432)
	viper.SetDefault("pg_user", "cardonciod")
	viper.SetDefault("pg_name", "cardoncio")

	// feature configuration
	viper.SetDefault("quote_inactivity_threshold_secs", 12.0*60.0*60.0)
	viper.SetDefault("admin_user_id", "")

	return DaemonOptions{
		CommonOpts: common.ReadConfigOrPanic(),
		ListenAddr: viper.GetString("listen_addr"),
		FeatureOptions: FeatureOptions{
			InactivityThresholdSecs: viper.GetFloat64("quote_inactivity_threshold_secs"),
			AdminUserID:             viper.GetString("admin_user_id"),
		},

		DatabaseOpts: DatabaseOptions{
			Host:     viper.GetString("pg_host"),
			Port:     viper.GetInt("pg_port"),
			User:     viper.GetString("pg_user"),
			Password: viper.GetString("pg_password"),
			Name:     viper.GetString("pg_name"),
		},
	}
}
