package cardonciod

import (
	"context"
	"database/sql"
	"fmt"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/olebedev/when"
	"github.com/olebedev/when/rules/en"
	"gitlab.com/hstefan/cardoncio/pkg/cardonciodb"
	"gitlab.com/hstefan/cardoncio/pkg/common"
	pb "gitlab.com/hstefan/cardoncio/proto/cardoncio"
	"google.golang.org/grpc"
)

var log = common.Log

type server struct {
	pb.UnimplementedChatEventConsumerServer
	db          *cardonciodb.Queries
	featureOpts FeatureOptions
}

func (s *server) persistChatChannel(ctx *context.Context, chatChannel *pb.ChatChannel) (string, error) {
	if _, err := s.db.PersistChannel(*ctx, chatChannel.ExternalId); err != nil {
		return "", fmt.Errorf("failed to persist chat channel: %v", err)
	}
	return chatChannel.ExternalId, nil
}

func (s *server) persistChatUser(ctx *context.Context, chatUser *pb.ChatUser) (*cardonciodb.User, error) {
	recChatUser, err := s.db.PersistUser(*ctx, cardonciodb.PersistUserParams{
		ID:          chatUser.ExternalId,
		DisplayName: sql.NullString{String: chatUser.DisplayName, Valid: true},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to persist chat user: %v", err)
	}
	return &recChatUser, nil
}

func (s *server) persistUserMessageChain(ctx *context.Context, userMsg *pb.UserMessage) error {
	if userMsg == nil {
		return nil
	}
	chatID, err := s.persistChatChannel(ctx, userMsg.GetChannel())
	if err != nil {
		return err
	}
	sender, err := s.persistChatUser(ctx, userMsg.Sender)
	if err != nil {
		return err
	}
	m := cardonciodb.PersistMembershipParams{
		GroupID: chatID,
		UserID:  sender.ID,
	}
	_, err = s.db.PersistMembership(*ctx, m)
	if err != nil {
		return err
	}
	return s.persistUserMessageChain(ctx, userMsg.ReplyTo)
}

func (s *server) checkQuoteTrigger(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	gu, err := s.db.FindMembership(*ctx, cardonciodb.FindMembershipParams{UserID: userMsg.Sender.ExternalId, GroupID: userMsg.Channel.ExternalId})
	if err != nil {
		return nil, err
	}
	if !gu.LastActive.Valid {
		return nil, nil
	}
	timeSinceActive := time.Now().UTC().Sub(gu.LastActive.Time)
	inactivityThreshold := s.featureOpts.InactivityThresholdSecs
	group, err := s.db.FindGroupById(*ctx, gu.GroupID)
	if err != nil {
		return nil, err
	}
	if group.QuoteInactivityThresholdMs.Valid {
		inactivityThreshold = float64(group.QuoteInactivityThresholdMs.Int64) / 1000.0
	}
	if timeSinceActive.Seconds() > inactivityThreshold {
		quotes, err := s.db.ListUserQuotesOrderedByLastSentOn(*ctx, gu.ID)
		if err != nil {
			return nil, err
		}
		if len(quotes) == 0 {
			return nil, nil
		}
		mention, err := formatUserMention(userMsg.Sender.ExternalId, userMsg.Sender.DisplayName)
		if err != nil {
			return nil, err
		}
		replyCmd, _ := replyToUserMessage(fmt.Sprintf("Wecome back, %s!", mention))
		if err = s.db.UpdateQuoteLastSentOn(*ctx, quotes[0].ID); err != nil {
			return nil, err
		}
		fwdCmd := pb.AnyCommand_ForwardMessage{ForwardMessage: &pb.ForwardMessageCommand{
			ChannelId: gu.GroupID, MessageId: quotes[0].MessageRef,
		}}
		return append(replyCmd, &pb.AnyCommand{Command: &fwdCmd}), nil
	}

	return []*pb.AnyCommand{}, nil
}

func (s *server) consumeUserMessage(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	preCmds, err := s.checkQuoteTrigger(ctx, userMsg)
	if err != nil {
		// intentionally does not cause a request to fail, just logs
		log.Errorf("failed checking quote trigger: %v", err)
	}

	// persisting user message will necessarily trigger update last_active to now()
	err = s.persistUserMessageChain(ctx, userMsg)
	if err != nil {
		return nil, err
	}
	log.Debugf("entities persisted for %+v", userMsg)

	if cmd := tryParseCmd(userMsg.Contents); cmd != nil {
		r, err := s.executeUserCommand(ctx, cmd, userMsg)
		if err != nil {
			return nil, fmt.Errorf("unable to execute user command %v (%v)", cmd, err)
		}
		return append(preCmds, r...), nil
	}

	return preCmds, nil
}

func (s *server) greetUser(sender *pb.ChatUser) ([]*pb.AnyCommand, error) {
	if len(sender.DisplayName) == 0 {
		return nil, fmt.Errorf("user \"%s\" has no display name", sender.ExternalId)
	}
	msg := fmt.Sprintf("Hello, %s!", sender.DisplayName)
	cmd := pb.AnyCommand_TextReply{TextReply: &pb.TextReplyCommand{Response: msg}}
	return []*pb.AnyCommand{{Command: &cmd}}, nil
}

func (s *server) replaceMessageWithContents(contents string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	ch := userMsg.Channel
	sendCmd := pb.SendMessageCommand{ChannelId: ch.ExternalId, Content: contents}
	deleteCmd := pb.DeleteMessageCommand{ChannelId: ch.ExternalId, MessageId: userMsg.ExternalId}

	cmds := []*pb.AnyCommand{
		{Command: &pb.AnyCommand_SendMessage{SendMessage: &sendCmd}},
		{Command: &pb.AnyCommand_DeleteMessage{DeleteMessage: &deleteCmd}},
	}
	return cmds, nil
}

func (s *server) sendMeMessage(args []string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	sr := userMsg.Sender
	if len(sr.DisplayName) == 0 {
		return nil, fmt.Errorf("user \"%s\" has no display name", sr.ExternalId)
	}
	c := fmt.Sprintf("<i>%s %s</i>", sr.DisplayName, strings.Join(args, " "))
	return s.replaceMessageWithContents(c, userMsg)
}

func formatUserMention(externalID, displayName string) (string, error) {
	if len(displayName) == 0 {
		return "", fmt.Errorf("user \"%s\" has no display name", externalID)
	}
	// FIXME: this is very "leaky" and relies on telegram-specific formatting
	return fmt.Sprintf("<a href=\"tg://user?id=%s\">%s</a>", externalID, displayName), nil
}

func (s *server) sendLennyMessage(args []string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	mention, err := formatUserMention(userMsg.Sender.ExternalId, userMsg.Sender.DisplayName)
	if err != nil {
		return nil, err
	}
	return s.replaceMessageWithContents(fmt.Sprintf("%s: <i>%s</i> ( ͡° ͜ʖ ͡°)", mention, strings.Join(args, " ")), userMsg)
}

func (s *server) sendShrugMessage(args []string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	mention, err := formatUserMention(userMsg.Sender.ExternalId, userMsg.Sender.DisplayName)
	if err != nil {
		return nil, err
	}
	return s.replaceMessageWithContents(fmt.Sprintf("%s: <i>%s</i> \\_(ツ)_/¯", mention, strings.Join(args, " ")), userMsg)
}

func (s *server) storeUserQuote(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	if userMsg.ReplyTo == nil {
		return replyToUserMessage("Quote command must me issued in response to a message.")
	}
	r := userMsg.ReplyTo
	groupUser, err := s.db.FindMembership(*ctx, cardonciodb.FindMembershipParams{UserID: r.Sender.ExternalId, GroupID: r.Channel.ExternalId})
	if err != nil {
		return nil, err
	}
	c := cardonciodb.CreateQuoteParams{
		GroupUserID: groupUser.ID,
		MessageRef:  userMsg.ReplyTo.ExternalId,
	}
	_, err = s.db.CreateQuote(*ctx, c)
	if err != nil {
		return nil, err
	}
	return replyToUserMessage("Quote created successfully!")
}

func (s *server) removeUserQuote(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	if userMsg.ReplyTo == nil {
		return replyToUserMessage("Quote command must me issued in response to the quote message.")
	}
	if userMsg.ReplyTo.Sender.ExternalId != userMsg.Sender.ExternalId {
		// maybe we should overengineer this later and add ACLs
		return replyToUserMessage("Quotes can only be removed by the user who has been quoted.")
	}
	r := userMsg.ReplyTo
	groupUser, err := s.db.FindMembership(*ctx, cardonciodb.FindMembershipParams{UserID: r.Sender.ExternalId, GroupID: r.Channel.ExternalId})
	if err != nil {
		return nil, err
	}
	rc, err := s.db.DeleteUserQuote(*ctx, cardonciodb.DeleteUserQuoteParams{GroupUserID: groupUser.ID, MessageRef: r.ExternalId})
	if rc == 0 {
		return replyToUserMessage("Quote not found.")
	}
	if rc > 1 {
		// this should never happen if the database constraints are working... and they likely should do that
		return nil, fmt.Errorf("attempt to remove %d quotes at once", rc)
	}
	return replyToUserMessage("Quote removed successfully!")
}

func (s *server) createReminder(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	if userMsg.ReplyTo == nil {
		return replyToUserMessage("/remindme command must me issued in response to a message.")
	}
	parser := when.New(nil)
	parser.Add(en.All...)

	dbUser, err := s.db.FindUserByID(*ctx, userMsg.Sender.ExternalId)
	if err != nil {
		return nil, nil
	}
	var now = time.Now()
	if dbUser.Timezone.Valid {
		loc, err := time.LoadLocation(dbUser.Timezone.String)
		if err != nil {
			return replyToUserMessage("Invalid timezone on database, re-run /settz.")
		}
		now = now.In(loc)
	}
	r, err := parser.Parse(userMsg.Contents, now)
	if err != nil {
		return nil, err
	}
	groupUser, err := s.db.FindMembership(*ctx, cardonciodb.FindMembershipParams{
		GroupID: userMsg.Channel.ExternalId, UserID: userMsg.Sender.ExternalId})
	if err != nil {
		return nil, err
	}
	_, err = s.db.CreateReminder(*ctx, cardonciodb.CreateReminderParams{
		GroupUserID:  groupUser.ID,
		MessageRef:   userMsg.ReplyTo.ExternalId,
		ScheduledFor: r.Time,
		SentOn:       sql.NullTime{},
	})
	if err != nil {
		log.Errorf("failed to create reminder: %v", err)
	}
	return replyToUserMessage(fmt.Sprintf("Reminder created sucessfully. %v", r.Time))
}

func (s *server) setTimezone(ctx *context.Context, args []string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	if len(args) != 1 {
		return replyToUserMessage("/settz command expects exactly one argument.")
	}
	loc, err := time.LoadLocation(args[0])
	if err != nil {
		return replyToUserMessage("Unable to parse IANA timezone")
	}
	dbTimezone := sql.NullString{String: loc.String(), Valid: true}
	if s.db.UpdateUserTimezone(*ctx, cardonciodb.UpdateUserTimezoneParams{ID: userMsg.Sender.ExternalId, Timezone: dbTimezone}) != nil {
		return nil, err
	}
	dateFormatted := time.Now().In(loc).Format(time.RFC3339)
	return replyToUserMessage(fmt.Sprintf("Timezone configured.\nLocal time: %s", dateFormatted))
}

func (s *server) getGroupTime(ctx *context.Context, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	channelMembers, err := s.db.ListChannelMembers(*ctx, userMsg.Channel.ExternalId)
	if err != nil {
		return nil, err
	}
	var outputMsg strings.Builder
	now := time.Now()
	for _, member := range channelMembers {
		var userDisplayName = member.ID
		if member.DisplayName.Valid {
			userDisplayName = member.DisplayName.String
		} else {
			log.Errorf("user %s has no display name stored", member.ID)
		}
		if member.Timezone.Valid {
			loc, err := time.LoadLocation(member.Timezone.String)
			if err != nil {
				log.Errorf("user %v has an unparseable timezone stored: %v", member.ID, member.Timezone)
				continue
			}
			dateFormatted := now.In(loc).Format(time.RFC3339)
			fmt.Fprintf(&outputMsg, "%s: %s\n", userDisplayName, dateFormatted)
		} else {
			fmt.Fprintf(&outputMsg, "%s: missing timezone\n", userDisplayName)
		}
	}
	if outputMsg.Len() == 0 {
		return replyToUserMessage("Group has no known members.")
	}
	return replyToUserMessage(outputMsg.String())
}

func (s *server) reconfigure(ctx *context.Context, args []string, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	if s.featureOpts.AdminUserID == "" || s.featureOpts.AdminUserID != userMsg.Sender.ExternalId {
		log.Warningf("attempt to reconfigure from %v", userMsg.Sender.ExternalId)
		return []*pb.AnyCommand{}, nil
	}
	if len(args) != 2 {
		return replyToUserMessage("expected two arguments")
	}
	switch args[0] {
	case "quote_inactivity_threshold":
		return s.updateQuoteInactivityThreshold(ctx, userMsg.Channel.ExternalId, args[1])
	default:
		log.Debugf("unsupported config key")
	}
	return []*pb.AnyCommand{}, nil
}

func (s *server) updateQuoteInactivityThreshold(ctx *context.Context, groupID, thresholdText string) ([]*pb.AnyCommand, error) {
	threshold, err := time.ParseDuration(thresholdText)
	if err != nil {
		return replyToUserMessage("unparseable argument")
	}
	if threshold.Nanoseconds() < 0 {
		return replyToUserMessage("must be a positive value")
	}
	nullableThreshold := sql.NullInt64{Int64: threshold.Milliseconds(), Valid: true}
	updateErr := s.db.UpdateGroupQuoteInactivityThreshold(*ctx,
		cardonciodb.UpdateGroupQuoteInactivityThresholdParams{ID: groupID, QuoteInactivityThresholdMs: nullableThreshold})
	if updateErr != nil {
		return nil, updateErr
	}
	return replyToUserMessage(fmt.Sprintf("Threshold set: %v", threshold))
}

func replyToUserMessage(msg string) ([]*pb.AnyCommand, error) {
	cmd := pb.AnyCommand_TextReply{TextReply: &pb.TextReplyCommand{Response: msg}}
	return []*pb.AnyCommand{{Command: &cmd}}, nil
}

func (s *server) executeUserCommand(ctx *context.Context, cmd *userCommand, userMsg *pb.UserMessage) ([]*pb.AnyCommand, error) {
	log.Infof("executing command %#v", cmd)
	switch cmd.Command {
	case "greet":
		return s.greetUser(userMsg.Sender)
	case "me":
		return s.sendMeMessage(cmd.Args, userMsg)
	case "lenny":
		return s.sendLennyMessage(cmd.Args, userMsg)
	case "shrug":
		return s.sendShrugMessage(cmd.Args, userMsg)
	case "quote":
		return s.storeUserQuote(ctx, userMsg)
	case "rmquote":
		return s.removeUserQuote(ctx, userMsg)
	case "remindme":
		return s.createReminder(ctx, userMsg)
	case "settz":
		return s.setTimezone(ctx, cmd.Args, userMsg)
	case "gtime":
		return s.getGroupTime(ctx, userMsg)
	case "cfg":
		return s.reconfigure(ctx, cmd.Args, userMsg)
	default:
		log.Debugf("unhandled command %#v", cmd.Command)
	}
	return []*pb.AnyCommand{}, nil
}

type userCommand struct {
	Command string
	Args    []string
}

func tryParseCmd(text string) *userCommand {
	cmdRe := regexp.MustCompile(`^/(\w*)@?\w*\s*(.*)$`)
	argRe := regexp.MustCompile(`([^"]\S*|".+?")\s*`)

	if !cmdRe.MatchString(text) {
		return nil
	}

	cmdFind := cmdRe.FindStringSubmatch(text)
	if len(cmdFind) != 3 {
		log.Fatalf("unexpected array returned by FindStringSubmatch")
	}
	cmd := cmdFind[1]
	if cmd == "" {
		return nil
	}

	if len(cmdFind[2]) != 0 {
		// we have a non-empty set of arguments
		parsedArgs := argRe.FindAllStringSubmatch(cmdFind[2], -1)
		args := make([]string, len(parsedArgs))
		for i, argMatch := range parsedArgs {
			// argMatch should be an array of two elements, full match and the argument itself
			args[i] = strings.Trim(strings.TrimSpace(argMatch[1]), "\"")
		}
		return &userCommand{
			Command: cmd,
			Args:    args,
		}
	}
	return &userCommand{
		Command: cmd,
		Args:    []string{},
	}
}

// ConsumeEvent implements helloworld.GreeterServer
func (s *server) ConsumeEvent(ctx context.Context, in *pb.ConsumeEventRequest) (*pb.ConsumeEventResponse, error) {
	switch x := in.Event.EventType.(type) {
	case *pb.ChatEvent_UserMessage:
		r, err := s.consumeUserMessage(&ctx, x.UserMessage)
		if err != nil {
			log.Errorf("%v", err)
			return nil, err
		}
		return &pb.ConsumeEventResponse{Commands: r}, nil
	}
	return &pb.ConsumeEventResponse{Commands: nil}, nil
}

func timestampProtoFromNullTime(ts sql.NullTime) (*timestamp.Timestamp, error) {
	if ts.Valid {
		tsPb, err := ptypes.TimestampProto(ts.Time)
		if err != nil {
			return nil, err
		} else {
			return tsPb, nil
		}
	} else {
		return nil, nil
	}
}
func (s *server) GetDueReminders(ctx context.Context, in *pb.GetDueRemindersRequest) (*pb.GetDueRemindersResponse, error) {
	dueReminders, err := s.db.FindDueReminders(ctx)
	if err != nil {
		return nil, err
	}
	var reminderProtos = make([]*pb.Reminder, len(dueReminders))
	for i, rem := range dueReminders {
		sentOn, err := timestampProtoFromNullTime(rem.SentOn)
		if err != nil {
			return nil, err
		}
		dueOn, err := ptypes.TimestampProto(rem.ScheduledFor)
		if err != nil {
			return nil, err
		}
		reminderProtos[i] = &pb.Reminder{
			Id:          strconv.Itoa(int(rem.ID)),
			DueOn:       dueOn,
			GroupId:     rem.GroupID,
			UserId:      rem.UserID,
			ReminderRef: rem.MessageRef,
			SentOn:      sentOn,
		}
	}
	return &pb.GetDueRemindersResponse{Reminders: reminderProtos}, nil
}

func (s *server) PutReminderSent(ctx context.Context, in *pb.PutReminderSentRequest) (*pb.PutReminderSentResponse, error) {
	reminderID, err := strconv.Atoi(in.ReminderId)
	if err != nil {
		return nil, err
	}
	if s.db.UpdateReminderSentOn(ctx, int32(reminderID)) != nil {
		return nil, err
	} else {
		return &pb.PutReminderSentResponse{}, nil
	}
}

// makeDatabaseConnectionPool returns a connection pool object
func makeDatabaseConnectionPool(databaseOpts *DatabaseOptions) *sql.DB {
	pool, err := sql.Open("postgres", databaseOpts.AsConnectionURL())
	if err != nil {
		common.Log.Fatalf("failed to initialize pg connection pool: %v", err)
	}
	return pool
}

// BlockOnServe starts the event consumer service and waits
func BlockOnServe(daemonOpts DaemonOptions) error {
	dbPool := makeDatabaseConnectionPool(&daemonOpts.DatabaseOpts)
	defer dbPool.Close()
	sdb := cardonciodb.New(dbPool)

	lis, err := net.Listen("tcp", daemonOpts.ListenAddr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterChatEventConsumerServer(s, &server{db: sdb, featureOpts: daemonOpts.FeatureOptions})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	return nil
}
