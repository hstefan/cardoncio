package cardonciod

import (
	"testing"
)

func stringsEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func mustParseAsExpected(t *testing.T, s string, expected *userCommand) {
	actual := tryParseCmd(s)
	if actual == nil && expected == nil {
		return
	}
	if (actual == nil && expected != nil) || (actual != nil && expected == nil) {
		t.Fatalf("expected parse result %#v, actual %#v", expected, actual)
	}
	if actual.Command != expected.Command {
		t.Fatalf("expected command %#v, actual %#v", expected.Command, actual.Command)
	}
	if !stringsEqual(actual.Args, expected.Args) {
		t.Fatalf("expected arguments %#v, actual %#v", expected.Args, actual.Args)
	}
}

func TestParseCommandNoArgs(t *testing.T) {
	expected := userCommand{
		Command: "greet",
		Args:    []string{},
	}
	mustParseAsExpected(t, "/greet", &expected)
}

func TestParseCommandWithUnquotedArgs(t *testing.T) {
	expected := userCommand{
		Command: "me",
		Args:    []string{"tests", "his", "code"},
	}
	mustParseAsExpected(t, "/me tests his code", &expected)
}

func TestParseCommandWithQuotedArgs(t *testing.T) {
	expected := userCommand{
		Command: "remindme",
		Args:    []string{"tomorrow at midnight", "test my code"},
	}
	mustParseAsExpected(t, "/remindme \"tomorrow at midnight\" \"test my code\"", &expected)
}

func TestParseCommandWithMixedArgs(t *testing.T) {
	expected := userCommand{
		Command: "meme",
		Args:    []string{"insanitywolf", "TEST MY CODE", "IN PROD"},
	}
	mustParseAsExpected(t, "/meme insanitywolf \"TEST MY CODE\" \"IN PROD\"", &expected)
}

func TestParseNotACommand(t *testing.T) {
	mustParseAsExpected(t, "a regular message", nil)
}

func TestParseEmptyCommandName(t *testing.T) {
	mustParseAsExpected(t, "/ ", nil)
}
