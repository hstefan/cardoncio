package cardonciod

import (
	"github.com/spf13/cobra"
	common "gitlab.com/hstefan/cardoncio/pkg/common"
)

// NewCardonciodCommand returns an initialized command for starting tgingestd
func NewCardonciodCommand() cobra.Command {
	rootCmd := cobra.Command{
		Use:   "cardonciod",
		Short: "The bot's heart",
		Long:  "Consume chat events from an arbitrary source and commands the publisher to react",
		Run: func(cmd *cobra.Command, args []string) {
			runDaemon(cmd)
		},
	}
	return rootCmd
}

func runDaemon(cmd *cobra.Command) {
	log := common.Log
	daemonOpts := ReadConfig()
	common.ReconfigureLog(daemonOpts.CommonOpts)
	log.Info("starting up")
	log.Debugf("settings: %+v", daemonOpts)
	BlockOnServe(daemonOpts)
}
