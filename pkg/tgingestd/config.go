package tgingestd

import (
	"github.com/spf13/viper"
	"gitlab.com/hstefan/cardoncio/pkg/common"
)

func init() {
	common.InitializeConfigSystem("tgingestd")
	viper.SetDefault("chat_event_consumer_addr", "localhost:5555")
}

// DaemonOptions is used to control running settings for tgingestd
type DaemonOptions struct {
	CommonOpts            common.CommonOptions
	APIToken              string
	ChatEventConsumerAddr string
}

// ReadConfig reads settings using viper and builds a DaemonOptions object
func ReadConfig() DaemonOptions {
	return DaemonOptions{
		CommonOpts:            common.ReadConfigOrPanic(),
		APIToken:              viper.GetString("api_token"),
		ChatEventConsumerAddr: viper.GetString("chat_event_consumer_addr"),
	}
}
