package tgingestd

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/hstefan/cardoncio/pkg/common"
	pb "gitlab.com/hstefan/cardoncio/proto/cardoncio"
	"google.golang.org/grpc"
)

var log = common.Log

func connectInsecureGrpc(addr string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("unable to connect to ChatEventConsumer: %v", err)
	}
	return conn, nil
}

func extractChatUserFromMessage(msg *tgbotapi.Message) pb.ChatUser {
	return pb.ChatUser{
		ExternalId:  strconv.FormatInt(msg.From.ID, 10),
		DisplayName: strings.TrimSpace(fmt.Sprintf("%s %s", msg.From.FirstName, msg.From.LastName)),
		UserName:    msg.From.UserName,
	}
}

func extractChatChannelFromMessage(msg *tgbotapi.Message) pb.ChatChannel {
	var displayName *string = nil
	if msg.Chat.IsGroup() {
		displayName = &msg.Chat.Title
	} else {
		displayName = &msg.Chat.UserName
	}
	return pb.ChatChannel{
		ExternalId:  strconv.FormatInt(msg.Chat.ID, 10),
		DisplayName: strings.TrimSpace(*displayName),
	}
}

func userMessageFromMessage(msg *tgbotapi.Message) *pb.UserMessage {
	sender := extractChatUserFromMessage(msg)
	channel := extractChatChannelFromMessage(msg)
	userMessage := pb.UserMessage{
		Sender:     &sender,
		Channel:    &channel,
		Contents:   msg.Text,
		ExternalId: strconv.Itoa(msg.MessageID),
	}
	if msg.ReplyToMessage != nil {
		userMessage.ReplyTo = userMessageFromMessage(msg.ReplyToMessage)
	}
	return &userMessage
}

func markReminderSent(cardoncio pb.ChatEventConsumerClient, reminderID string) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if _, err := cardoncio.PutReminderSent(ctx, &pb.PutReminderSentRequest{ReminderId: reminderID}); err != nil {
		log.Errorf("unable to mark reminder as sent: %v", err)
	}
}

func pollDueReminders(bot *tgbotapi.BotAPI, cardoncio pb.ChatEventConsumerClient) {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		r, err := cardoncio.GetDueReminders(ctx, &pb.GetDueRemindersRequest{})
		if err != nil {
			log.Errorf("error while polling reminders: %v", err)
			time.Sleep(time.Minute)
			continue
		}
		for _, reminder := range r.Reminders {
			channelID, err := strconv.ParseInt(reminder.GroupId, 10, 64)
			if err != nil {
				log.Errorf("error parsing group id: %v", err)
				continue
			}
			msgID, err := strconv.ParseInt(reminder.ReminderRef, 10, 64)
			if err != nil {
				log.Errorf("error parsing message id: %v", err)
				continue
			}
			fwdReq := tgbotapi.NewForward(channelID, channelID, int(msgID))
			if _, err := bot.Send(fwdReq); err != nil {
				log.Errorf("failed to forward message: %v", err)
			} else {
				log.Info("reminder sent to user")
			}
			go markReminderSent(cardoncio, reminder.Id)
		}
		time.Sleep(time.Second)
	}
}

// BlockOnEventLoop polls Telegram APIs forever and publishes internal events
func BlockOnEventLoop(options DaemonOptions) error {
	bot, err := tgbotapi.NewBotAPI(options.APIToken)
	if err != nil {
		return err
	}

	log.Infof("dialing to grpc://%v", options.ChatEventConsumerAddr)
	conn, err := connectInsecureGrpc(options.ChatEventConsumerAddr)
	if err != nil {
		return err
	}
	defer conn.Close()
	log.Infof("connected, initializing ChatEventConsumerClient")
	chatEventConsumer := pb.NewChatEventConsumerClient(conn)
	go pollDueReminders(bot, pb.NewChatEventConsumerClient(conn))

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)
	for update := range updates {
		log.Debugf("update: %+v", update)
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Infof("[%s] %s", update.Message.From.UserName, update.Message.Text)

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		userMessage := userMessageFromMessage(update.Message)

		chatEvt := pb.ChatEvent{Timestamp: nil, EventType: &pb.ChatEvent_UserMessage{UserMessage: userMessage}}
		r, err := chatEventConsumer.ConsumeEvent(ctx, &pb.ConsumeEventRequest{Event: &chatEvt})
		if err != nil {
			log.Errorf("chat event consumer request failed: %v", err)
		}

		log.Debugf("got: %v", r)
		for _, cmd := range r.Commands {
			switch cmd := cmd.Command.(type) {
			case *pb.AnyCommand_TextReply:
				log.Debugf("replying to %d: %s", update.Message.Chat.ID, cmd.TextReply.Response)
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, cmd.TextReply.Response)
				msg.ReplyToMessageID = update.Message.MessageID
				msg.ParseMode = tgbotapi.ModeHTML
				bot.Send(msg)
			case *pb.AnyCommand_SendMessage:
				log.Debugf("sending message %#v", cmd.SendMessage)
				c := cmd.SendMessage
				channelID, err := strconv.ParseInt(c.ChannelId, 10, 64)
				if err != nil {
					log.Errorf("failed to parse channelID %v as integer: %v", c.ChannelId, err)
					break
				}
				msg := tgbotapi.NewMessage(channelID, c.Content)
				msg.ParseMode = tgbotapi.ModeHTML
				bot.Send(msg)
			case *pb.AnyCommand_DeleteMessage:
				c := cmd.DeleteMessage
				channelID, err := strconv.ParseInt(c.ChannelId, 10, 64)
				if err != nil {
					log.Errorf("failed to parse channelID %v as integer: %v", c.ChannelId, err)
					break
				}
				messageID, err := strconv.ParseInt(c.MessageId, 10, 32)
				if err != nil {
					log.Errorf("failed to parse messageID %v as integer: %v", c.MessageId, err)
					break
				}
				msg := tgbotapi.NewDeleteMessage(channelID, int(messageID))
				bot.Send(msg)
				continue
			case *pb.AnyCommand_ForwardMessage:
				c := cmd.ForwardMessage
				channelID, err := strconv.ParseInt(c.ChannelId, 10, 64)
				if err != nil {
					log.Errorf("failed to parse channelID %v as integer: %v", c.ChannelId, err)
					break
				}
				messageID, err := strconv.ParseInt(c.MessageId, 10, 32)
				if err != nil {
					log.Errorf("failed to parse messageID %v as integer: %v", c.MessageId, err)
					break
				}
				msg := tgbotapi.NewForward(channelID, channelID, int(messageID))
				bot.Send(msg)
				continue
			default:
				log.Info("commanded to do the unknown")
			}
		}
	}

	return nil
}
