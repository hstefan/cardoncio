package tgingestd

import (
	"github.com/spf13/cobra"
	"gitlab.com/hstefan/cardoncio/pkg/common"
)

// NewTgIngestdCommand returns an initialized command for starting tgingestd
func NewTgIngestdCommand() cobra.Command {
	rootCmd := cobra.Command{
		Use:   "tgingestd",
		Short: "Telegram event ingestion service",
		Long:  "Polls TelegramAPIs and massages the events to Cardoncio's shared format",
		Run: func(cmd *cobra.Command, args []string) {
			runDaemon(cmd)
		},
	}
	return rootCmd
}

func runDaemon(cmd *cobra.Command) {
	daemonOpts := ReadConfig()
	common.ReconfigureLog(daemonOpts.CommonOpts)
	log := common.Log
	log.Info("starting up")
	log.Debugf("settings: %+v", daemonOpts)
	if err := BlockOnEventLoop(daemonOpts); err != nil {
		log.Panic(err)
	}
}
